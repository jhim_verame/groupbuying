# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import time
import math
import decimal
from decimal import Decimal
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from precursor.models import *
import simplejson as json
from django.shortcuts import redirect

@login_required
def index(request):
    '''return index'''
    return render(request, 'precursor/base.html')

def task(request):
    '''return task'''
    participant = get_participant(request.user)
    item_len = len(Item.objects.all())
    current_task = get_current_task(participant)

    if current_task:
        #store first time that proposer sees task
        if not current_task.offer_time and participant.role == 'proposer':
            current_task.offer_time = time.time()
            current_task.save()

        #proper way to round financial values:
        #https://stackoverflow.com/questions/17470883/
        #how-to-round-to-two-decimal-places-in-python-2-7
        #min_offer = current_task.item.groupbuy_price / 10
        #max_offer = current_task.item.groupbuy_price * Decimal(0.9)

        min_offer = current_task.item.groupbuy_price - current_task.item.market_price
        max_offer = current_task.item.market_price
        current_task.qty_to_buy = current_task.item.groupbuy_pack / 2
        current_task.total_market_price = current_task.item.market_price * current_task.qty_to_buy
        offer_range = max_offer - min_offer
        context = {
            'participant': participant,
            'task': current_task,
            'completed_tasks': int(current_task.item.i_id - 1),
            'total_task': item_len,
            'hasOffer': True if current_task.offer > 0 else False,
            'min_offer': min_offer,
            'step': offer_range / 10,
            'max_offer': max_offer
        }

        return render(request, 'precursor/task.html', context)

    else:

        item_len = len(Item.objects.all())
        context = {
            'participant': participant,
            'completed_tasks': item_len,
            'total_task': item_len
        }

    return render(request, 'precursor/end.html', context)

def get_participant(current_user):
    '''get participant'''
    return Participant.objects.get(user=current_user)

def get_pairing(current_p):
    '''return the pairing'''
    return Pairing.objects.get(proposer=current_p) if current_p.role == 'proposer' else Pairing.objects.all().get(responder=current_p)

def get_task_by_id(participant, item_id):
    '''get a task by its id'''
    pairing = get_pairing(participant)
    item = Item.objects.get(i_id=item_id)
    return Task.objects.filter(pairing=pairing, item=item).first()

def get_current_task(participant):
    '''
    This function takes the latest task of the user and checks if it has been completed.
    If so, create a new task.
    '''
    pairing = get_pairing(participant)
    tasks = Task.objects.all().filter(pairing=pairing)
    task_no = len(tasks)
    items = Item.objects.all()

    current_task = get_task_by_id(participant, task_no if task_no > 0 else 1)

    if current_task and not current_task.response:
        return current_task

    else:
        if task_no < len(items):
            task_no += 1
            current_item = items.get(i_id=task_no)
            return Task.objects.create(pairing=pairing, item=current_item)

    return None

def check_offer(request):
    '''function to check if an offer has been made'''
    json_data = json.loads(request.POST['data'])
    participant = get_participant(request.user)
    current_task = get_task_by_id(participant, json_data['task_id'])

    if current_task.offer > 0:
        return HttpResponse('true')

    return HttpResponse('false')

def get_offer(request):
    '''
	retrieve a made offer and return to the responder
    '''
    json_data = json.loads(request.POST['data'])
    participant = get_participant(request.user)
    current_task = get_task_by_id(participant, json_data['task_id'])
    item = current_task.item
    to_pay = item.groupbuy_price - current_task.offer
    to_pay_percent = int(round((to_pay / item.market_price) * 100))
    context = {
        'task': current_task,
        'participant': participant,
        'to_pay': to_pay,
        'to_pay_percent': to_pay_percent,
        'other_percent': 100 - to_pay_percent,
        'savings': item.market_price - to_pay
    }

	#record time as responder gets offer for the first time
    if not current_task.respond_time:
        current_task.respond_time = time.time()
        current_task.save()

    current_task.total_market_price = current_task.item.market_price * (item.groupbuy_pack / 2)

    return render(request, 'precursor/respond.html', context)


def make_offer(request):
    '''
	add an offer made by the proposer
    '''
    json_data = json.loads(request.POST['data'])
    participant = get_participant(request.user)
    current_task = get_task_by_id(participant, json_data['task_id'])
    current_task.offer = json_data['offer']
    #record total time taken for proposer to make an offer
    current_task.offer_time = int(time.time() - current_task.offer_time)
    current_task.save()


    context = {
        'task': current_task,
        'participant': participant
    }

    return render(request, 'precursor/loading.html', context)

def check_response(request):
    '''function to check if responder has responded'''
    json_data = json.loads(request.POST['data'])
    participant = get_participant(request.user)
    current_task = get_task_by_id(participant, json_data['task_id'])

    if current_task.response is None:
        return HttpResponse('false')

    return HttpResponse('true')

def get_response(request):
    ''' function to get response from responder '''
    json_data = json.loads(request.POST['data'])
    participant = get_participant(request.user)
    current_task = get_task_by_id(participant, json_data['task_id'])
    item = current_task.item
    qty_to_buy = item.groupbuy_pack / 2
    #this is the saving of the proposer
    if current_task.response == 'accepted':
        saving = item.market_price - current_task.offer
    else:
        saving = 0

    participant = get_participant(request.user)
    participant.reward += Decimal(saving)
    participant.save()

    context = {
        'task': current_task,
        'saving': saving,
        'participant': get_participant(request.user)
    }

    return render(request, 'precursor/response.html', context)

def make_response(request):
    '''
	    function to respond to proposer's offer
    '''
    json_data = json.loads(request.POST['data'])
    participant = get_participant(request.user)
    current_task = get_task_by_id(participant, json_data['task_id'])
    current_task.response = json_data['response']
    current_task.respond_time = int(time.time() - current_task.respond_time)
    current_task.save()
    item = current_task.item
    qty_to_buy = item.groupbuy_pack / 2
    #this is the saving of the responder
    if current_task.response == 'accepted':
        saving = item.market_price - (item.groupbuy_price - current_task.offer)
    else:
        saving = 0

    participant = get_participant(request.user)
    participant.reward += Decimal(saving)
    participant.save()

    context = {
        'task': current_task,
        'participant': participant,
        'saving': saving
    }
    return render(request, 'precursor/response.html', context)
