from django.conf.urls import url

from . import views
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^get-task/$', views.task, name='task'),
    url(r'^make-offer/$',views.make_offer, name='make_offer'),
    url(r'^check-response/$',views.check_response, name='check_response'),
    url(r'^get-response/$',views.get_response, name='get_response'),
    url(r'^check-offer/$',views.check_offer, name='check_offer'),
    url(r'^get-offer/$',views.get_offer, name='get_offer'),
    url(r'^make-response/$',views.make_response, name='make_response'),
    url(r'^accounts/login/$', auth_views.LoginView.as_view(template_name='precursor/login.html'), name='auth_login'),
    url(r'^admin/', admin.site.urls),
]