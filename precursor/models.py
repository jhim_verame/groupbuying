# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Participant(models.Model):
    user = models.ForeignKey(User)
    reward = models.DecimalField(default=0, max_digits=4, decimal_places=2)

    CONDITION_CHOICES = (
        ('transparent', 'transparent'),
        ('not transparent', 'not transparent')
    )

    ROLE_CHOICES = (
        ('proposer', 'proposer'),
        ('responder', 'responder')
    )

    condition = models.CharField(
        max_length=20,
        choices=CONDITION_CHOICES,
        default='not transparent'
    )

    role = models.CharField(
        max_length=10,
        choices=ROLE_CHOICES,
        default='proposer'
    )

    def __str__(self):
        return self.user.username

class Pairing(models.Model):
    proposer = models.ForeignKey(Participant, related_name='proposer')
    responder = models.ForeignKey(Participant, related_name='responder')


    def save(self, *args, **kwargs):
        if self.proposer.user.id == self.responder.user.id:
            return str('Proposer cannot be the responder')
        else:
            super(Pairing, self).save(*args, **kwargs)

    def __str__(self):
        return str('%s & %s') %(self.proposer, self.responder)

class Item(models.Model):
    i_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=20)
    groupbuy_price = models.DecimalField(max_digits=4, decimal_places=2)
    market_price = models.DecimalField(max_digits=4, decimal_places=2)
    quantity = models.CharField(max_length=5, default='1kg')
    groupbuy_pack = models.IntegerField(default=1)
    #budget = models.DecimalField(max_digits=4,decimal_places=2)

    def __str__(self):
        return self.name

class Task(models.Model):
    item = models.ForeignKey(Item)
    pairing = models.ForeignKey(Pairing)
    offer_time = models.IntegerField(null=True)
    respond_time = models.IntegerField(null=True)
    offer = models.DecimalField(max_digits=4, decimal_places=2, default=0)
    response = models.CharField(max_length=10, null=True)

    def __str__(self):
        return str('Task %s (%s)') %(self.item.i_id, self.pairing)
