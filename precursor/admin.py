# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(Participant)
#admin.site.register(Question)
admin.site.register(Item)
admin.site.register(Task)
admin.site.register(Pairing)