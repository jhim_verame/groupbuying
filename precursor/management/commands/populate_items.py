from django.core.management.base import BaseCommand, CommandError
from precursor.models import *
import csv


class Command(BaseCommand):

    def handle(self, *args, **options):
        #here we populate the items
        for par in Participant.objects.all():
            par.reward = 0
            par.save()
        Item.objects.all().delete()
        Task.objects.all().delete()
        items_file = 'precursor/tasks/items.csv'

        gb_total = 0
        mp_total = 0

        with open(items_file) as csvfile:
            reader = csv.DictReader(csvfile)
            for i, row in enumerate(reader):
                gb_total += float(row['market_price']) - (float(row['groupbuy_price']) / 2)
                mp_total += float(row['groupbuy_price']) - float(row['market_price'])
                Item.objects.create(
                    i_id = i+1,
                    name = row['name'],
                    groupbuy_price = row['groupbuy_price'],
                    market_price = row['market_price'],
                    quantity = row['quantity'],
                    groupbuy_pack = row['groupbuy_pack']
                )

        print "expected reward (gb): %f" % (gb_total)
        print "expected reward (mp): %f" % (mp_total)
        print 'Done populating items'