from django.core.management.base import BaseCommand, CommandError
from precursor.models import *
from django.contrib.auth.models import User

class Command(BaseCommand):

    def add_arguments(self, parser):
            # Positional arguments
            parser.add_argument('condition')

    def handle(self, *args, **options):
        #here we add a new pair of participants
        #transparent / not_transparent
        condition = options['condition'].replace('_',' ')
        options = ['transparent', 'not transparent']

        if condition not in options:
            print 'Incorrect condition (either transparent or not_transparent)'
            return

        user_len = len(User.objects.all())

        #create 2 new users, 2 new participants
        for i in xrange(user_len, user_len+2):
            
            new_user = User.objects.create(
                username='user%d' %(i),
            )

            new_user.set_password('user%duser%d' % (i, i))
            new_user.save()

            if i == user_len:
                proposer = Participant.objects.create(
                    user=new_user,
                    condition=condition,
                    role='proposer'
                )
            else:
                responder = Participant.objects.create(
                    user=new_user,
                    condition=condition,
                    role='responder'
                )

        #create pairing
        Pairing.objects.create(
            proposer = proposer,
            responder = responder
        )

        print 'Done adding new pair: %s and %s' % (proposer.user.username, responder.user.username)