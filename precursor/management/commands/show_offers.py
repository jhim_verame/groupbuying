from django.core.management.base import BaseCommand, CommandError
from precursor.models import *
import csv, os

def create_task_csv():
    '''lets create csv for task'''

    exceptions = ['user3','user23']

    with open('tasks.csv', 'w') as csvfile:

        headers = ['task_no', 'proposer', 'responder', 'wholesale_price', 'rrp', 'proposer_pay', 'responder_pay', 'proposer_pay_pt', 'responder_pay_pt', 'response']
        writer = csv.DictWriter(csvfile, fieldnames=headers)
        writer.writeheader()

        for idx, task in enumerate(Task.objects.all()):
            if task.pairing.proposer.user.username in exceptions:
                continue
            #print "%2d) offer: %.02f, response: %s, proposer: %.02f, responder: %.02f" % (task.item.i_id, offer, task.response, task.offer, task.item.groupbuy_price - task.offer)
            
            #task_no, proposer, responder, wholesale_price, rrp, proposer_pay, responder_pay, proposer_pay_pt, responder_pay_pt, response

            responder_pay = task.item.groupbuy_price - task.offer
            proposer_pay_pt = (task.offer / task.item.groupbuy_price) * 100
            responder_pay_pt = (responder_pay / task.item.groupbuy_price) * 100

            writer.writerow(
                {
                    'task_no': idx,
                    'proposer': task.pairing.proposer,
                    'responder': task.pairing.responder,
                    'wholesale_price': task.item.groupbuy_price,
                    'rrp': task.item.market_price,
                    'proposer_pay': task.offer,
                    'responder_pay': responder_pay,
                    'proposer_pay_pt': '%0.2f' % (proposer_pay_pt),
                    'responder_pay_pt': '%0.2f' % (responder_pay_pt),
                    'response': task.response
                }
            )
            
class Command(BaseCommand):

    def handle(self, *args, **options):
        create_task_csv()

        print 'Done'
        