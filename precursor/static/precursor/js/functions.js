var start = function(name) {
	$('.task-body').load('/precursor/get-task/', function(data){

		var step = parseFloat($('.main').data('step'), 10);
		var min_offer = parseFloat($('.main').data('min-offer'), 10);
		var max_offer = parseFloat($('.main').data('max-offer'), 10);
		var budget = parseFloat($('.main').data('budget'), 10);
		
		//initialise slider
		$('.slider').ionRangeSlider({
			type: "single",
			min: min_offer,
			max: max_offer,
			step: step,
			grid: true,
			grid_snap: true,
			prefix: "£",
			force_edges: true,
			onChange: function(data) {
				$('.irs-single').removeClass('hidden');
				$('.irs-slider').removeClass('hidden');
				$('.irs-bar-edge').removeClass('hidden');
				$('.irs-single').html('You pay: £'+ parseFloat($('.slider').val(), 10).toFixed(2) +', other person pays: £' + ( parseFloat($('.main').data('gb-price'), 10) - parseFloat($('.slider').val(), 10)).toFixed(2));
				$('.btn-offer').removeAttr('disabled');
				$('.irs-min').html('£'+min_offer.toFixed(2));
				$('.irs-max').html('£'+max_offer.toFixed(2));
			},
			onStart: function(data) {
				$('.irs-single').addClass('hidden');
				$('.irs-slider').addClass('hidden');
				$('.irs-bar-edge').addClass('hidden');
				for ( var i = 0; i < 11; i++){
					var usr_split=i;
					var other_split=10-i;
					var grid_text = parseFloat($('.js-grid-text-'+i).html(), 10);
					var saving = budget - grid_text; 
					$('.js-grid-text-'+i).html('£' + saving.toFixed(2));
				}
			}
		});

		$('.irs-min').html('£'+min_offer.toFixed(2));
		$('.irs-max').html('£'+max_offer.toFixed(2));
		$('.irs-min').css('visibility','visible');
		$('.btn-offer').click(make_offer);

		//Function bound on slider to calculate what "other participant" is paying
		// $( ".slider" ).on( "change", function( event, ui ) {
		// 	var other_cost = (parseFloat($('.main').data('budget'), 10) - parseFloat($(this).spinner('value'), 10)).toFixed(2);
		// 	$('.cost-split').html('Other person pays: £'+other_cost);
		// 	$('.btn-offer').removeAttr('disabled');

		// });

	});
};

// function to retrieve cookie (from django)
var getCookie = function(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

// Function to check if the responder has made a response to the current proposer's offer
var check_response = function(){
	 var task_data = {
    	'task_id': $('.main').attr('data-current-task')
    };
	// keep checking for a response

	$.ajax({
		url: '/precursor/check-response/',
		data: {'data': JSON.stringify(task_data), 'csrfmiddlewaretoken': getCookie('csrftoken')},
		type: 'POST',
		success: function(data){
			if(data==='true'){
				// load response.html
				$('.user-panel').load('/precursor/get-response/', {'data': JSON.stringify(task_data), 'csrfmiddlewaretoken': getCookie('csrftoken')});
			}else{
				setTimeout(check_response, 5000);
			}
		}
	});
};

// Function to check if the proposer has made an offer
var check_offer = function(){
	 var task_data = {
    	'task_id': $('.main').attr('data-current-task')
    };
	// keep checking for a response

	$.ajax({
		url: '/precursor/check-offer/',
		data: {'data': JSON.stringify(task_data), 'csrfmiddlewaretoken': getCookie('csrftoken')},
		type: 'POST',
		success: function(data){
			if(data==='false'){
				setTimeout(check_offer, 5000);
			}else{
				// load response.html
				$('.user-panel').load('/precursor/get-offer/', {'data': JSON.stringify(task_data), 'csrfmiddlewaretoken': getCookie('csrftoken')});
			}
		}
	});
};

//Function to store proposer's offer
var make_offer = function(){

    var data = {
    	'offer': parseFloat($('.slider').val(), 10).toFixed(2),
    	'task_id': $('.main').attr('data-current-task'),
    	'time_to_offer': $('.offer-timer').html()
    };

    $('.user-panel').load('/precursor/make-offer/', {'data': JSON.stringify(data), 'csrfmiddlewaretoken': getCookie('csrftoken')});

};

var make_response = function(){

	var data = {
		'task_id': $('.main').attr('data-current-task'),
		'response': $(this).attr('data-response'),
		'time_to_respond': $('.offer-timer').html()
	};

	$('.user-panel').load('/precursor/make-response/', {'data': JSON.stringify(data), 'csrfmiddlewaretoken': getCookie('csrftoken')});

};

$( function() {
  start();
});